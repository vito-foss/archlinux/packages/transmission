# Contributor : Tom Gundersen <teg@jklm.no>
# Contributor : Ionut Biru <ibiru@archlinux.org>
# Contributor : Thomas Weißschuh <thomas t-8ch de>
# Contributor : Florian Pritz <bluewind@xinu.at>

pkgname=transmission-gtk
pkgdesc='Fast, easy, and free BitTorrent client (GTK+ GUI)'
pkgver=4.0.6
pkgrel=9
arch=(x86_64)
url="http://www.transmissionbt.com/"
license=(GPL-2.0-or-later)
depends=(
  curl
  libevent
  gtk3
  gtkmm3
  libappindicator-gtk3
  hicolor-icon-theme
  libb64
  miniupnpc
  libnatpmp
  libdeflate
)
optdepends=('libnotify: Desktop notification support')
makedepends=(
  cmake
  curl
  dht
  gtk3
  gtkmm3
  libappindicator-gtk3
  libb64
  libdeflate
  libevent
  libnatpmp
  miniupnpc
  ninja
)
source=("https://github.com/transmission/transmission/releases/download/$pkgver/transmission-$pkgver.tar.xz"
        febfe49c.patch)
sha256sums=('2a38fe6d8a23991680b691c277a335f8875bdeca2b97c6b26b598bc9c7b0c45f'
            '1e5917c79a0c17595f18b544c5c1ab101ecbef5b2ffb0ca42a0a3b221a85e044')

prepare() {
  cd transmission-$pkgver
  patch -p1 -i ../febfe49c.patch # Fix build with miniupnpc 2.2.8
}

build() {
  export CFLAGS+=" -ffat-lto-objects"
  cd transmission-$pkgver

  cmake \
    -G Ninja \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DENABLE_CLI=OFF \
    -DENABLE_DAEMON=OFF \
    -DENABLE_GTK=ON \
    -DENABLE_MAC=OFF \
    -DENABLE_QT=OFF \
    -DREBUILD_WEB=OFF \
    -DENABLE_TESTS=ON \
    -DENABLE_UTILS=OFF \
    -DENABLE_UTP=ON \
    -DINSTALL_LIB=OFF \
    -DUSE_SYSTEM_B64=ON \
    -DUSE_SYSTEM_DEFLATE=ON \
    -DUSE_SYSTEM_DHT=ON \
    -DUSE_SYSTEM_EVENT2=ON \
    -DUSE_SYSTEM_MINIUPNPC=ON \
    -DUSE_SYSTEM_NATPMP=ON \
    -DUSE_SYSTEM_PSL=ON \
    -DUSE_SYSTEM_UTP=OFF \
    -DUSE_GTK_VERSION=3 \
    -DWITH_APPINDICATOR=ON \
    -DWITH_CRYPTO=openssl \
    -S . \
    -B build

  cmake --build build --config Release
}

check() {
  cd transmission-$pkgver

  cd build
  ctest --output-on-failure -j "$(nproc)"
}

package() {
  cd transmission-$pkgver

  install -Dm644 COPYING "$pkgdir/usr/share/licenses/transmission-gtk/COPYING"

  cd build
  DESTDIR="$pkgdir" ninja gtk/install po/install
}
